package ma.oauth2.login.service;

import java.util.Map;

import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;

import ma.oauth2.login.dto.LocalUser;
import ma.oauth2.login.dto.UserRegistrationForm;
import ma.oauth2.login.exception.UserAlreadyExistAuthenticationException;
import ma.oauth2.login.model.User;

/**
 * @author Ousama
 */
public interface UserService {

	public User registerNewUser(UserRegistrationForm UserRegistrationForm)
			throws UserAlreadyExistAuthenticationException;

	User findUserByEmail(String email);

	LocalUser processUserRegistration(String registrationId, Map<String, Object> attributes, OidcIdToken idToken,
			OidcUserInfo userInfo);
}